#!/bin/sh
# SPDX-License-Identifier: MIT

: "${BASESUITE:=unstable}"
OURSUITE=dpkgroot
WORKDIR=$(mktemp -d)
# shellcheck disable=SC2034 # unused in install.sh
PATCHDIR=$(realpath patches)
REPREPRO_BASE_DIR=$(realpath repo)
HTTP_PORT=7251
: "${MIRROR:=http://deb.debian.org/debian}"
SOURCE_DATE_EPOCH=$(date +%s)

export SOURCE_DATE_EPOCH
export REPREPRO_BASE_DIR
HTTPD_PID=

# to prevent dch stalling with
# dch: Did you see those 2 warnings?  Press RETURN to continue...
export DEBEMAIL='robot <dpkg@root.demo>'

cleanup() {
	if test -n "$HTTPD_PID"; then
		kill "$HTTPD_PID"
	fi
	if test -d "$WORKDIR"; then
		rm -Rf "$WORKDIR"
	fi
}

trap cleanup EXIT

python3 -m http.server --bind 127.0.0.1 --directory "$REPREPRO_BASE_DIR" "$HTTP_PORT" &
HTTPD_PID=$!

SRC_LIST_PATCHED="deb [ trusted=yes ] http://127.0.0.1:$HTTP_PORT/ $OURSUITE main"
