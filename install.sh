#!/bin/sh
# SPDX-License-Identifier: MIT
# Too many false positives:
# shellcheck disable=SC2016

set -u
set -e

. ./setup.sh

: "${VARIANT:=essential}"
: "${USE_FAKEROOT:=}"
: "${ARCH:=$(dpkg --print-architecture)}"
: "${INCLUDE:=}"

# we have to run both mmdebstrap invocations inside another mmdebstrap because:
#
# a) we don't want to run chrootless without chroot isolation
# b) we want to be able to use the patched mmdebstrap
# c) the apt user on the outside might have a different uid from inside a fresh
#    mmdebstrap chroot -- to make sure that both tarballs contain the same apt
#    uid, we run both mmdebstrap invocations from within the same chroot
#    https://bugs.debian.org/969631
#
# We have to create /var/cache/ldconfig to achieve bit-by-bit identical
# tarballs between fakechroot and chrootless modes because /var/cache/ldconfig
# is never created in chrootless mode as ldconfig of libc-bin.postinst is run
# from the outside with -r and thus doesn't leave its cache inside the chroot
#
# We delete and re-create /var/log/journal if it exists (when installing
# systemd) because extended attributes cannot be preserved under fakeroot.
#
# The outer mmdebstrap is in auto mode so either unshare or root (on salsaci)
# is chosen. This means that unless USE_FAKEROOT is set, the inner mmdebstrap
# calls will run as root (unshared or real). The outer mmdebstrap must not be
# run in fakechroot mode because fakechroot cannot be nested.
mmdebstrap "$BASESUITE" \
	--architectures="$(dpkg --print-architecture),$ARCH" \
	--variant="$VARIANT" \
	--mode=auto \
	--include="mmdebstrap,fakeroot,arch-test,dpkg-dev,libfakeroot:$ARCH,$INCLUDE,adduser" \
	--customize-hook='chroot "$1" adduser --gecos user --disabled-password user' \
	--customize-hook='chroot "$1" mmdebstrap --architecture="'"$ARCH"'" --verbose '"${INCLUDE:+--include=$INCLUDE}"' --mode=auto --variant="'"$VARIANT"'" --hook-dir=/usr/share/mmdebstrap/hooks/merged-usr ${USE_FAKEROOT:+--customize-hook="'"if [ -d \\\$1/var/log/journal ]; then rmdir \\\$1/var/log/journal; mkdir --mode=2755 \\\$1/var/log/journal; chroot \\\$1 chown root:systemd-journal /var/log/journal; fi"'"} "'"$BASESUITE"'" /tmp/normal.tar "'"$SRC_LIST_PATCHED\" \"$MIRROR\"" \
	--customize-hook='copy-out /tmp/normal.tar .' \
	--customize-hook='rm "$1"/tmp/normal.tar' \
	--customize-hook='tar --one-file-system -C "$1" -cf "$1"/before.tar ./usr ./bin ./etc ./lib ./sbin ./var' \
	--customize-hook='copy-out /before.tar .' \
	--customize-hook='rm "$1"/before.tar' \
	--customize-hook='chroot "$1" ${USE_FAKEROOT:+runuser -u user -- fakeroot} mmdebstrap --architecture="'"$ARCH"'" --verbose '"${INCLUDE:+--include=$INCLUDE}"' --mode=chrootless --variant="'"$VARIANT"'" --hook-dir=/usr/share/mmdebstrap/hooks/merged-usr ${USE_FAKEROOT:+--customize-hook="'"mkdir --mode=700 \\\$1/var/cache/ldconfig"'"} "'"$BASESUITE"'" /tmp/chrootless.tar "'"$SRC_LIST_PATCHED\" \"$MIRROR\"" \
	--customize-hook='copy-out /tmp/chrootless.tar .' \
	--customize-hook='rm "$1"/tmp/chrootless.tar' \
	--customize-hook='tar --one-file-system -C "$1" -cf "$1"/after.tar ./usr ./bin ./etc ./lib ./sbin ./var' \
	--customize-hook='copy-out /after.tar .' \
	--customize-hook='rm "$1"/after.tar' \
	/dev/null \
	"$SRC_LIST_PATCHED" \
	"$MIRROR"

# make sure that the outer system didn't change
cmp before.tar after.tar || diffoscope before.tar after.tar

# compare the normal and the chrootless tarball
cmp normal.tar chrootless.tar || diffoscope normal.tar chrootless.tar
