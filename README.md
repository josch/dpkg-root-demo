What is this?
=============
It is something between a proof-of-concept of what can be done with `DPKG_ROOT`
and a collection of pending work to be upstreamed to make it work in unstable.

The `patches/` folder does not contains normal patches. Instead it contains
scripts named after the relevant source package to be patched. Each script is
supposed to be run inside the source tree of the relevant source package and to
perform the necessary changes. `debian/changelog` is not to be modified here
and done elsewhere.

`update.sh` is the entry point. Running it will create a repository of patched
binary packags. It is configured using environment variables in variables.sh.
Upon completion there will be a `reprepro`-managed repository in the `repo`
folder.

`install.sh` uses the repo to build an essential-only chroot with and without
using chrootless mode and compares the resulting tarballs for equality.

Requirements
============

It should be run on a Debian system that has a `deb-src` entry for `unstable`
in its `sources.list`. It requires the following packages:
 * `devscripts` (for `dch` and `chdist`)
 * `python3` (to spawn a little webserver)
 * `reprepro`
 * `sbuild` is used to perform the builds and needs a chroot called `sid` configured
